# images-bed

blog图床

## 搭建过程

### 配置PicGo-Core(推荐)

1.   PicGo-Core安装

     -   Typora 【偏好设置】->【图像】->【上传服务设定】内，【上传服务】选择【PicGo-Core】，然后点击【下载或更新】

     -   node npm安装(偏好这种) 版本`1.5.0-alpha.0`

         ```bash
         npm install picgo -g
         ```
         **需要选择【上传服务】为【Custom Command】，配置命令`picgo upload`**

2.   安装插件(npm全局安装直接命令行输入就可)

     ```bash
     picgo install super-prefix
     picgo install gitee-uploader
     ```
     -   [gitee-uploader](https://github.com/lizhuangs/picgo-plugin-gitee-uploader)
     -   [super-prefix](https://github.com/gclove/picgo-plugin-super-prefix)
     -   [gitee](https://github.com/zhanghuid/picgo-plugin-gitee)
     -   [github-plus](https://github.com/zWingz/picgo-plugin-github-plus)（同时支持GitHub和Gitee）
     
3.   配置文件

     [PicGo-Core文档](https://picgo.github.io/PicGo-Core-Doc/zh/guide/getting-started.html)
     
     1.   使用`picgo set uploader`命令来交互自动生成配置文件
     
     2.   配置json文件参考
     
          ```json
          {
            "picBed": {
              "current": "gitee",
              "gitee": {
                "repo": "Forgo7ten/images-bed",
                "branch": "master",
                "token": "[gitee token]",
                "customPath": "yearMonth",
                "path": "$customPath",
                "customUrl": ""
              },
              "uploader": "gitee"
            },
            "picgoPlugins": {
              "picgo-plugin-super-prefix": true,
              "picgo-plugin-gitee-uploader": true
            },
            "picgo-plugin-super-prefix": {
              "fileFormat": "YYYYMMDDHHmmss"
            }
          }
          ```
          
     





### 配置PicGo.app

1.   下载安装[PicGo](https://github.com/Molunerfinn/PicGo/releases)
2.   打开后在【插件设置】中搜索`gitee`安装插件`gitee`或者`gitee-uploader`
3.   gitee新建token

     1.   gitee个人头像选择【设置】
     2.   【安全设置】中选择【私人令牌】
     3.   新建私人令牌，只勾选`user_info`和`projects`即可
4.   配置gitee并设置为默认图床
     1.   图床中选择【gitee图床】（安装插件才有）
     2.   配置相应设置：repo、token等等

5.   根据[Typora使用PicGo.app文档](https://support.typora.io/Upload-Image/#picgoapp-chinese-language-only)，在【PicGo设置】查看【设置Server】是否已开启，并确定端口为`36677`（默认就是

配置Typora 【偏好设置】->【图像】->【上传服务设定】内，选择【PicGo(app)】，然后设置PicGo路径



配置好后可点击【验证图片上传选项】，可以看是否上传成功

